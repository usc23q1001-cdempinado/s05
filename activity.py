from abc import ABC, abstractmethod

class Animal(ABC):
  #constructor
  def __init__(self, name, breed, age):
    self._name=name
    self._breed=breed
    self._age=age
  #getters and setters  
  def getName(self):
    return self._name
  def getBreed(self):
    return self._breed
  def getAge(self):
    return self._age
  def setName(self, name):
    self._name=name
  def setBreed(self, breed):
    self._breed=breed
  def setAge(self, age):
    self._age=age
  #abstract classes
  @abstractmethod
  def eat(self,food):
    pass
  def make_sound(self):
    pass

class Dog(Animal):
  def eat(self,food):
    print(f"Eaten {food}")
  def make_sound(self):
    print("Bark! Woof! Arf!")
  def call(self):
    print(f"Here {super().getName()}!")

class Cat(Animal):
  def eat(self,food):
    print(f"Serve me {food}")
  def make_sound(self):
    print("Miaow! Nyaw! Nyaaaaa")
  def call(self):
    print(f"{super().getName()}, come on!")

dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1=Cat("Puss","Persian",4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()