class Person:
  def __init__(self):
    self._name = "Dave"
    self._age = 0
  def set_age(self, age):
    self._age = age
  def get_age(self):
    print(f"Age of Person: {self._age}")
  def set_name(self, name):
    self._name  = name
  def get_name(self):
    return self._name

class Student(Person):
  
  def __init__(self,student_no,course,year_lvl):
    super().__init__()
    self._student_no = student_no
    self._course = course
    self._year_lvl = year_lvl
  def set_student_no(self, student_no):
    self._student_no = student_no
  def get_student_no(self):
    return self._student_no
  def set_course(self, course):
    self._course = course
  def get_course(self):
    return self._course
  def set_year_lvl(self, year_lvl):
    self._year_lvl = year_lvl
  def get_year_lvl(self):
    return self._year_lvl
  def get_detail(self):
    print(f"{self.get_name()} is currently in year {self.get_year_lvl()} taking up {self.get_course()}")

s1 = Student(2019,"BSIT",1)
s1.set_name("John")
s1.get_detail()
# p1 = Person()
# p2 = Person()
# p1.set_age(25)
# p1.set_name("John")
# p2.set_age(30)
# p2.set_name("Mary")
# p1.get_name()
# p1.get_age()
# p2.get_name()
# p2.get_age()


  
  